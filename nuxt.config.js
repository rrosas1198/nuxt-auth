import { join } from "path";

const resolveCwd = (...segments) => join(process.cwd(), ...segments);

const IS_DEVELOPMENT = process.env.NODE_ENV === "development";

const URL_BASE = "https://cms.ximehoyosfit.com/api/";
const GOOGLE_CLIENT_ID = "477997440535-dga2dhh1s9c5kl259m6k8icvk7a6pek4.apps.googleusercontent.com";
const FACEBOOK_CLIENT_ID = "161378709305985";

export default {
    dev: IS_DEVELOPMENT,
    ssr: false,
    target: "static",
    telemetry: false,
    head: {
        htmlAttrs: { lang: "es" },
        title: "Ximena Hoyos",
        titleTemplate: "Ximena Hoyos - %s"
    },
    loading: false,
    components: false,
    css: [],
    serverMiddleware: [],
    plugins: [],
    buildModules: ["@nuxt/typescript-build", "@nuxtjs/pwa"],
    modules: ["@nuxtjs/axios", "@nuxtjs/auth-next"],
    axios: {
        baseUrl: "https://cms.ximehoyosfit.com/api/",
        debug: IS_DEVELOPMENT
    },
    pwa: {
        manifest: {
            lang: "es"
        }
    },
    auth: {
        redirect: {
            home: "/profile",
            login: "/signin",
            logout: "/signin",
            callback: "/e0ba89f"
        },
        strategies: {
            local: {
                token: {
                    property: "token"
                },
                user: {
                    property: "user"
                },
                endpoints: {
                    login: {
                        url: `${URL_BASE}login-social`,
                        method: "post"
                    },
                    logout: false,
                    user: {
                        url: `${URL_BASE}current`,
                        method: "get"
                    }
                }
            },
            facebook: {
                clientId: FACEBOOK_CLIENT_ID,
                scope: ["public_profile", "email"],
                redirectUri: "/e0ba89f",
                endpoints: {
                    authorization: "https://facebook.com/v2.12/dialog/oauth",
                    userInfo:
                        "https://graph.facebook.com/v6.0/me?fields=email,first_name,id,last_name,picture{url}"
                }
            },
            google: {
                clientId: GOOGLE_CLIENT_ID,
                endpoints: {
                    authorization: "https://accounts.google.com/o/oauth2/auth",
                    userInfo: "https://www.googleapis.com/oauth2/v3/userinfo"
                },
                scope: ["profile", "email"],
                redirectUri: "/e0ba89f",
                responseType: "token id_token",
                codeChallengeMethod: ""
            }
        }
    },
    watch: [],
    build: {
        devTools: IS_DEVELOPMENT,
        extractCSS: !IS_DEVELOPMENT,
        optimizeCSS: !IS_DEVELOPMENT,
        optimization: {
            minimize: !IS_DEVELOPMENT
        },
        transpile: [],
        terser: {
            terserOptions: {
                compress: {
                    drop_console: !IS_DEVELOPMENT
                }
            }
        }
    }
};
