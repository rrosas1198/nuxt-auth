const {
    mergeEslintConfig,
    EslintBaseConfig,
    EslintPrettierConfig,
    EslintTsConfig
} = require("@techkit/linter-config");

module.exports = mergeEslintConfig(EslintBaseConfig, EslintTsConfig, EslintPrettierConfig, {
    parserOptions: {
        sourceType: "module"
    },
    overrides: [
        {
            rules: {
                "@typescript-eslint/indent": "off",
                "@typescript-eslint/ban-types": "off"
            }
        }
    ]
});
