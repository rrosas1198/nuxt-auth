import { Oauth2Scheme } from "~auth/runtime";

const GOOGLE_CLIENT_ID = "477997440535-dga2dhh1s9c5kl259m6k8icvk7a6pek4.apps.googleusercontent.com";

const GoogleDefaults = {
    clientId: GOOGLE_CLIENT_ID,
    endpoints: {
        authorization: "https://accounts.google.com/o/oauth2/auth",
        userInfo: "https://www.googleapis.com/oauth2/v3/userinfo"
    },
    scope: ["profile", "email"],
    responseType: "token id_token",
    codeChallengeMethod: ""
};

export default class GoogleScheme extends Oauth2Scheme {
    constructor($auth, options, ...defaults) {
        super($auth, options, ...defaults, GoogleDefaults);
    }
}
