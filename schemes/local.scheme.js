import { LocalScheme as AuthLocalScheme } from "~auth/runtime";

const LocalDefaults = {
    endpoints: {
        login: {
            url: `${process.env.URL_BASE}login-social`,
            method: "post"
        },
        logout: false,
        user: {
            url: `${process.env.URL_BASE}current`,
            method: "get"
        }
    }
};

export default class LocalScheme extends AuthLocalScheme {
    constructor($auth, options, ...defaults) {
        super($auth, options, ...defaults, LocalDefaults);
    }
}
