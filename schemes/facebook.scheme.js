import { Oauth2Scheme } from "~auth/runtime";

const FACEBOOK_CLIENT_ID = "161378709305985";

const FacebookDefaults = {
    clientId: FACEBOOK_CLIENT_ID,
    endpoints: {
        authorization: "https://facebook.com/v2.12/dialog/oauth",
        userInfo:
            "https://graph.facebook.com/v6.0/me?fields=email,first_name,id,last_name,picture{url}"
    },
    scope: ["public_profile", "email"]
};

export default class FacebookScheme extends Oauth2Scheme {
    constructor($auth, options, ...defaults) {
        super($auth, options, ...defaults, FacebookDefaults);
    }
}
